class Guirlande{
    constructor(width, height, coeff){
        this.width = width;
        this.height = height;
        this.coeff = coeff;
        this.leds = this.initLeds();
    }
    initLeds(){
        let nb = this.width * this.height;
        let leds = new Array(nb);
        for (let idx = 0; idx < nb; idx++){
            leds[idx] = `#${randColor()}`;
            //leds[idx] = "#ffffff"
        }
        return leds;
    }
}

const myGuirlande = new Guirlande(10, 25, 40);

const mouse = {
    "isDown" : false
}

const loc = window.location;
const hostname = loc.hostname;
const wsport = parseInt(loc.port) + 1;

const socket = new WebSocket(`ws://${hostname}:${wsport}`);


function refreshCam(){
  let b = document.getElementsByClassName("rancy-mode-button")[0];
  let m = b.dataset.mode
  console.log(b,m);
  if (m != "camera"){
    return
  }
  socketmessage(socket, 'mode', 'camera');
}

// Connection opened
socket.addEventListener('open', (event) => {
    socketmessage(socket, 'status', 'Hello Server!');

    let applets = document.getElementsByClassName("rancy-guirlande");
    if (typeof applets == "undefined"){
        console.error(`div with class 'rancy-guirlande' is missing `);
    }
    console.warn("Element applet", applets);

    for (let appidx=0; appidx<applets.length; appidx++) {
        let canvas = document.createElement("canvas");
        canvas.addEventListener('mousemove', (event) => {
            if (mouse.isDown){
                let pos = getMousePos(canvas, event);
                let index = posToIdx(pos);
                myGuirlande.leds[index] = document.getElementById(`rancy-color-${appidx}`).value;
                drawCtx(ctx);
            }
        });
        canvas.addEventListener('mousedown', (event) => {
            mouse.isDown = true;
        });
        canvas.addEventListener('mouseup', (event) => {
            socketmessage(socket, 'data', myGuirlande.leds);
            mouse.isDown = false;
        });
        canvas.addEventListener('click', (event) => {
            let pos = getMousePos(canvas, event);
            let index = posToIdx(pos);
            myGuirlande.leds[index] = document.getElementById(`rancy-color-${appidx}`).value;
            socketmessage(socket, 'data', myGuirlande.leds);
            drawCtx(ctx);
        });
        const ctx = canvas.getContext('2d');
        let coeff = myGuirlande.coeff;
        canvas.width = myGuirlande.width * coeff;
        canvas.height = myGuirlande.height * coeff;
        canvas.id = `rancy-canvas-${appidx}`;
        
        let colorPicker = document.createElement("div");
        let colorInput = document.createElement("input");
        let colorLabel = document.createElement("label");
        colorPicker.id = `rancy-color-container-${appidx}`;
        colorPicker.className = `rancy-color-container`;
        colorInput.type = "color";
        colorInput.id = `rancy-color-${appidx}`;
        colorInput.name = `rancy-color-${appidx}`;
        colorInput.value = `#${randColor()}`;
        colorLabel.htmlFor = `rancy-color-${appidx}`;
        colorLabel.innerText = "Choisi ta couleur";
        colorPicker.appendChild(colorInput);
        colorPicker.appendChild(colorLabel);

        let clearButton = document.createElement("Button");
        clearButton.className = `rancy-clear-button`;
        clearButton.textContent = "Effacer";
        clearButton.onclick = ()=>{
            reset(ctx);
            socketmessage(socket, 'data', myGuirlande.leds);
        }

        let modeButton = document.createElement("Button");
        modeButton.className = `rancy-mode-button`;
        modeButton.textContent = "Passer en mode 📷"
        modeButton.dataset.mode = "draw"
        modeButton.onclick = ()=>{
          if (modeButton.dataset.mode == 'draw'){
              modeButton.dataset.mode = "camera"
              canvas.style.display = "none";
              colorPicker.style.display = "none";
              clearButton.style.display = "none";
              modeButton.textContent = "Passer en mode ✎";
              setInterval(refreshCam,500);
          } else if (modeButton.dataset.mode == 'camera'){
              socketmessage(socket, 'mode', 'draw');
              modeButton.dataset.mode = "draw"
              canvas.style.display = null;
              colorPicker.style.display = null;
              clearButton.style.display = null;
              modeButton.textContent = "Passer en mode 📷";
              clearInterval(refreshCam)
          }
        }
        
        applets[appidx].appendChild(modeButton);
        applets[appidx].appendChild(canvas);
        applets[appidx].appendChild(colorPicker);
        applets[appidx].appendChild(clearButton);
        socketmessage(socket, 'mode', 'draw');
        socketmessage(socket, 'data', myGuirlande.leds);
        drawCtx(ctx);
    }
});

// Listen for messages
socket.addEventListener('message', (event) => {
    console.log('Message from server ', event.data);
});

function randColor(){
    let colorString = Math.floor(Math.random()*16777215).toString(16);
    let colorStringLen = colorString.length;
    for (let i=colorStringLen; i<6; i++){
      colorString = "0" + colorString;
    }
    return colorString;
}

function getMousePos(canvas, evt) {
    let rect = canvas.getBoundingClientRect();
    return {
        x: (evt.clientX - rect.left) / (rect.right - rect.left) * canvas.width,
        y: (evt.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height
    };
}

function posToIdx(pos){
    if (Math.floor(pos.x/myGuirlande.coeff)%2 == 0){
        return myGuirlande.height * Math.floor(pos.x/myGuirlande.coeff) + Math.floor(pos.y/myGuirlande.coeff);
    }
    return myGuirlande.height * Math.floor(pos.x/myGuirlande.coeff) - Math.floor(pos.y/myGuirlande.coeff) + myGuirlande.height - 1;
}

function idxToPos(idx){
    if (Math.floor(idx/myGuirlande.height)%2 == 0){
        return {
            x: (Math.floor(idx/myGuirlande.height)) * myGuirlande.coeff,
            y: idx % myGuirlande.height * myGuirlande.coeff
        }
    } else {
        return {
            x: (Math.floor(idx/myGuirlande.height)) * myGuirlande.coeff,
            y: (myGuirlande.height - idx % myGuirlande.height - 1) * myGuirlande.coeff
        }
    }
}

function drawCtx(ctx){
    drawLeds(ctx);
    drawGrid(ctx);
}

function drawGrid(ctx){
    let coeff = myGuirlande.coeff;
    for (let i=0;i<=myGuirlande.width;i++){
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.moveTo(i* coeff, 0);
        ctx.lineTo(i* coeff, myGuirlande.height * coeff);
        ctx.stroke(); 
    }
    for (let j=0;j<=myGuirlande.height;j++){
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.moveTo(0, j* coeff);
        ctx.lineTo(myGuirlande.width * coeff, j* coeff);
        ctx.stroke(); 
    }
}

function drawLeds(ctx){
    myGuirlande.leds.forEach((led, idx)=>{
        ctx.fillStyle = led;
        let pos = idxToPos(idx);
        ctx.fillRect(pos.x, pos.y, myGuirlande.coeff, myGuirlande.coeff);
    })
}

function reset(ctx){
    myGuirlande.leds = myGuirlande.initLeds();
    drawCtx(ctx)
}

function socketmessage(socket, type, data){
    let cleanData = data
    if (type == "data"){
      cleanData = pixelToRGB(data)
    }
    socket.send(
        JSON.stringify(
            {
                "type":type,
                "data":cleanData
            }
        )
    );
}

function hexToRGB(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    // result [R,G,B]
    return result ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
    ] : null;
}

function pixelToRGB(data){
    let dataRGB = data.map(pixel=>hexToRGB(pixel))
    return dataRGB
}
